<?php 	
	$nCharge = 0;
	$tab = 1;
	if (isset($_GET['nCharge'])) {
		$nCharge = $_GET['nCharge'];
		$tab = $_GET['tab'];
	}

	$data = $referrals->selectCostumers();
	$costumer = $data['costumer'];
	$list = $data['list'];
	//print_r($data['list']);
 ?>
<div class="mainContainer">
<div class="clear"></div>
<div class="containerPanel">
	<div class="headContent">
		<h4 class="text-bold">Generador de remisiones</h4>
	</div>
	<div class="mainContent">
		<div class="clear"></div>
		<div class="clear"></div>
		<?php if (!isset($_GET['nCharge'])){?>
		<div class="row">	
			<div class="col-lg-2 ">	
				<div class="block h40 ">	
					<div class="middle content">
						<label for="nCharge">Numero de embarques</label>
					</div>
				</div>	
			</div>
			<div class="col-lg-1">	
				<select name="n" id="nCharge" class="inputStyle" onchange="nCharge(this);">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select>
			</div>
		</div>
		<?php } ?>
		<ul class="tabs"> 
		<?php 	
			$x = 1;
			while( $x <= $nCharge ) { ?>
		   		<li class="<?= ($tab == $x) ? 'current' : null ; ?> tabP" id=""> 
				 	<a href="?nCharge=<?= $nCharge?>&tab=<?= $x?>">Remision <?= $x?></a>
				 </li>
		    
		<?php $x++; }  ?>
		</ul>
		<ul class="containerTabs">
	
			<li class="containerTable">

				<table style="max-width:750px; border: 2px double #000; padding: 10px; width: 100%; height: 100px; background: #f9f9f9; font-family: Arial; font-size: 15px; background: url(../IMG/logo2.png); background-position: center; background-size: 100%;">
				<thead>
						
						<tr>
							<th>
								<h4 style="font-family: 15px !important;">REMISION DE SALIDA</h4>
							</th>
						</tr>
						<tr>
							<th>
							<table style="font-size: 14px; border-bottom: 2px solid #000;" width="100%;" >
								<tr>
									<th style="text-align: left; " width="70%">
										<table width="100%">
											<tr>
												<th width="32%" style="text-align: right;">
													<span style="padding:3px;"> <b>EMPRESA	:</b></span><br>
													<span style="padding:3px;"> <b>RFC	:</b></span><br>
													<span style="padding:3px;"> <b>DOMICILIO	:</b></span><br>
													<span style="padding:3px;"> <b>CIUDAD	:</b></span><br>
												</th>
												<th width="68%" style="text-align: left;">
													<span style="font-weight: normal; padding:3px;"> 
													<select name="employe" id="employe" onchange="getEmploye(this);" class="selectEmploye">
														<option value=""><?=  $datos['name'];?></option>
														<option value="4">El Cegador SPR de RL de CV</option>
														<option value="3">Las Albardas SPR de RL de CV</option>
														<option value="1">El Calabacillal SPR de RL de CV</option>
													</select>
													</span><br>
													<span style="font-weight: normal; padding:3px;">
														<input type="text" name="RFC" id="" value="<?= $datos['rfc'];?>" class="w100"></b>
													</span><br>
													<span style="font-weight: normal; padding:3px;">
														<input type="text" name="addres" id="" value="<?= $datos['addres'];?>" class="w100"></b>
													</span><br>
													<span style="font-weight: normal; padding:3px;">
														<input type="text" name="city" id="" value="<?= $datos['city'];?>" class="w100"></b>
													</span><br>
												</th>
											</tr>
										</table>
									</th>

									<th style="text-align: left;" width="30%">
										<table width="100%">
											<tr>
												<th width="50%" style="text-align: right;">
													<span> <b>FECHA	:</b></span><br>
													<span> <b>HORA	:</b></span><br>
													<span> <b>TEL	:</b></span><br><br>
													
												</th>
												<th width="50%" style="text-align: left;">
													<span style="font-weight: normal;">20<?= date('y-m-d');?></span><br>
													<span style="font-weight: normal;"><?= date('h-m');?></span><br>
													<span style="font-weight: normal;">842 104 31 96</span><br><br>
													
												</th>
											</tr>
										</table>
									</th>

								</tr>
							</table>
							</th>
						</tr>
				</thead>

		<tbody>
		
			<tr>
				<th>
					
				<table style="font-size: 14px; border-bottom: 2px solid #000; " width="100%;" >
					<tr>
						<th style="text-align: left; " width="100%">
							<table width="100%">
								<tr>
									<th width="20%" style="text-align: right;">
										<div style="padding: 2px;"> 
											<b>CLIENTE	:</b>
										</div>
										<div style="padding: 2px;"> 
											<b>DOMICILIO	:</b>
										</div>
										<div style="padding: 2px;"> 
											<b>TELEFONO	:</b>
										</div>
										<div style="padding: 2px;"> 
											<b>RFC	:</b>
										</div>
										
								
									</th>
									<th width="70%" style="text-align: left;">
										<div style="padding: 2px;">
										 	<span style="font-weight: normal;">
										 		<select name="costumer" id="costumer" onchange="getCostumer(this);">
													<option value="<?= $costumer['id'];?>"><?= $costumer['name'];?></option>
													<?php while($row = mysqli_fetch_array($list)){ ?>
														<option value="<?= $row['id'];?>"><?= $row['name']; ?></option>
														
														<?php } ?>
												</select>
										 		
										 	</span>
										</div>
									
	
										<div style="padding: 2px;">
										 	<span style="font-weight: normal;">
										 		<input type="text" name="name_costumer" id="name_costumer" value="<?= $costumer['adress'];?>">
										 		
										 	</span>
										</div>
										<div style="padding: 2px;">
											<span style="font-weight: normal;">
												<input type="text" name="name_costumer" id="name_costumer" value="<?= $costumer['phone'];?>">
												
										 	</span>
											
										</div>
										<div style="padding: 2px;">
											<span style="font-weight: normal;">
												<input type="text" name="name_costumer" id="name_costumer" value="<?= $costumer['rfc'];?>">
										 	</span>
										</div>
										
										
									</th>
								</tr>
							</table>
						</th>
						
					</tr>
				</table>
				</th>

			</tr>
			<!--TABLA DE CONTENIDO-->
			<tr width="100%">
				<th style="height: 250px; vertical-align: top;">
					<button onclick="openModal(this,'searchP');" class="btn blue s14">	
						Agregar Productos
					</button>
					<table style=" margin-top: 10px;"  width="100%" border="0">
						<thead style=" ">
							<tr style=""  width="100%" >
								<td style="padding: 2px; border-bottom: 1px solid #000; margin: 0px; ">
									Cantidad
								</td>
								<td style="padding: 2px; border-bottom: 1px solid #000;  ">
									Concepto
								</td>
								<td style="padding: 2px; border-bottom: 1px solid #000;  ">
									Precio
								</td>
								<td style="padding: 2px; border-bottom: 1px solid #000;  ">
									Importe
								</td>
							</tr>
						</thead>

						<tr style="">
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									900
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									Repollo verde arpillas de 50 lbs
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$2
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$1800
								</span>
							</th>
						</tr>

						<tr style="">
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									900
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									Repollo verde arpillas de 50 lbs
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$2
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$1800
								</span>
							</th>
						</tr>

						<tr style="">
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									900
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									Repollo verde arpillas de 50 lbs
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$2
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$1800
								</span>
							</th>
						</tr>

						<tr style="">
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									900
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									Repollo verde arpillas de 50 lbs
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$2
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$1800
								</span>
							</th>
						</tr>

						<tr style="">
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									900
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									Repollo verde arpillas de 50 lbs
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$2
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$1800
								</span>
							</th>
						</tr>

						<tr style="">
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									900
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									Repollo verde arpillas de 50 lbs
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$2
								</span>
							</th>
							<th style="padding: 2px; border-bottom: .5px solid #ccc;">
								<span style="font-weight: normal; font-size: 14px; ">
									$1800
								</span>
							</th>
						</tr>

					</table>

				</th>
			</tr>

			<!--TABLA DE CONTENIDO-->
			<tr>	
				<th>	
					<table width="100%" style=" border-bottom: .5px solid #000;">	
						<tr style="  width: 100%;" >

								<td style="	 border-top: 1px solid #000; border-bottom: 1px solid #000;" width="20%">
									<span style="font-weight: bold;">	Total bultos:</span>
								</td>

								<td style="padding: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000;" width="10%">
									<span style="font-weight: bold;">	900</span>
								</td>

								<td style="padding: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000;" width="10%">
									<span style="font-weight: bold; color: white">	</span>
								</td>

								<td style="padding: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000;" width="10%">
									<span style="font-weight: bold; color: white;">	</span>
								</td>

								<td style="padding: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000;" width="10%">
									<span style="font-weight: bold; color: white;">	</span>
								</td>

								<td style="text-align: right; padding: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000;" width="10%">
									Subtotal:
								</td>

								<td style="padding: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000;" width="10%">
									$1800
								</td>
						</tr>
						
						<tr style="  width: 100%;" >

								<td style="" width="20%">
									<span style="font-weight: bold;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white;">	</span>
								</td>

								<td style="text-align: right; font-size: 14px;" width="10%">
									SEGURO:
								</td>

								<td style="font-size: 14px;" width="10%">
									$0.00
								</td>
						</tr>

						<tr style="  width: 100%;" >

								<td style="" width="20%">
									<span style="font-weight: bold;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white;">	</span>
								</td>

								<td style="text-align: right; font-size: 14px;" width="10%">
									I.V.A:
								</td>

								<td style="font-size: 14px;" width="10%">
									$0.00
								</td>
						</tr>
						<tr style="  width: 100%;" >

								<td style="" width="20%">
									<span style="font-weight: bold;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white;">	</span>
								</td>

								<td style="text-align: right; font-size: 14px;" width="10%">
									OTROS:
								</td>

								<td style="font-size: 14px;" width="10%">
									$0.00
								</td>
						</tr>
						<tr style="  width: 100%;" >

								<td style="" width="20%">
									<span style="font-weight: bold;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white;">	</span>
								</td>

								<td style="" width="10%">
									<span style="font-weight: bold; color: white;">	</span>
								</td>

								<td style="text-align: right; font-size: 14px;"  width="10%">
									TOTAL:
								</td>

								<td style=" font-size: 14px;" width="10%">
									$0.00
								</td>
						</tr>
					</table>
				</th>
			</tr>

		</tbody>

		<footer>
			<tr>
				<th>
					<br>
					<table width="100%">
						<tr>
							<th width="40%" style="text-align: left; padding: 0px 15px;">
								<span>Chofer: Jose Carlos Morin Riojas</span>
								
								
							</th>
							<th width="30%">
								<span>Edad: 38</span>
							</th>
							<th width="30%">
								<span>Tel: 871 784 84 s47</span>
							</th>
						</tr>
					</table>
					<table width="100%">
						<tr>
							<th width="33.33%" style="padding: 15px; text-align: left;">
								<span>Tractor</span>
								<table width="100%">
									
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">Marca:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">KW</span>
										</th>
									</tr>
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">Modelo:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">2007</span>
										</th>
									</tr>
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">N economico:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">2007</span>
										</th>
									</tr>
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">PLACAS:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">WK-2390</span>
										</th>
									</tr>
								</table>

							</th>
							<th width="33.33%" style="padding: 15px; text-align: left;">
								<span>Caja</span>
								<table width="100%">
									
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">Tipo de caja:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">Termo</span>
										</th>
									</tr>
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">Placas:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">AS-3SSW</span>
										</th>
									</tr>
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">N economico:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">123</span>
										</th>
									</tr>
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">Temperatura:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">50 f</span>
										</th>
									</tr>
								</table>

							</th>
							<th width="33.33%" style="padding: 15px; text-align: left; vertical-align: top;">
								<span>Otros</span>
								<table width="100%">
								
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">CAAT:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">1245</span>
										</th>
									</tr>
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">ALPHA:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">ASRH</span>
										</th>
									</tr>
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">ICCMX:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">577442</span>
										</th>
									</tr>
									<tr>
										<th width="50%">
											<span style="font-weight: 400; font-size: 14px;">US DOT:</span>
										</th width="50%">
										<th>
											<span style="font-weight: 400; font-size: 14px;">155972Z</span>
										</th>
									</tr>
							
								</table>
							</th>
						</tr>
					</table>
				</th>
			</tr>

			<tr>
				<th>
					<table width="100%">
						<tr>
							<th width="45%" style="padding: 15px;">
								<div style="border-top: 1px solid #000; margin-top: 45px;"></div><div style="padding: 2px;"></div>
								<span style="font-weight: 300; padding: 5px; margin: 2px; font-size: 13px;">Persona que formula</span>
							</th>
							<th width="45%" style="padding: 15px;">
								<div style="border-top: 1px solid #000; margin-top: 45px;"></div><div style="padding: 2px;"></div>
								<span style="font-weight: 300; padding: 5px; margin: 2px; font-size: 13px;">Persona que recibe</span>
							</th>
						</tr>
					</table>
				</th>
			</tr>

		</footer>
	</table>
			</li>
			
			
			
		</ul>

		<button class="btn Green s14" onclick="openModal(this,'saveGroup');">
			<b>Generar remision</b>
		</button>
		<div class="clear"></div>
		<div class="clear"></div>

			

	</div>
	
	
    
	
</div>

</div>
<div class="clear"></div>

<form class="modal hidden" id="searchP">
	<div class="headModal">
		<h4 class="text-bold">Agregar productos </h4>
	</div>
	<div class="mainModal">
		<table>	
			<thead>	
				<tr>
					<td>Id</td>
					<td>Nombre</td>
					<td>Cantidad</td>
					<td>Unidad</td>
					<td>Acciones</td>

				</tr>
			</thead>
			<tbody>	
				<tr>
					<th>Id</th>
					<th>Nombre</th>
					<th>Cantidad</th>
					<th>Unidad</th>
					<th>
						<button>	
							Agregar
						</button>
					</th>
				</tr>
				<tr>
					<th>Id</th>
					<th>Nombre</th>
					<th>Cantidad</th>
					<th>Unidad</th>
					<th>
						<button>	
							Agregar
						</button>
					</th>
				</tr>
			</tbody>
		</table>
	</div>
</form>

<div class="bgBlack hidden" onclick="closeModal();"></div>
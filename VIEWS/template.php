<?php namespace Views;
	session_start();

	$template = new template();

	class template{

		public function __construct(){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ADMINISTRACION</title>
  	<link rel="stylesheet" href="<?= URL?>/VIEWS/resources/CSS/helpers.css">
	<link rel="stylesheet" href="<?= URL?>/VIEWS/resources/CSS/main.css">
	<link rel="stylesheet" href="<?= URL?>/VIEWS/resources/CSS/jquery-ui.min.css">

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:600" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,500,500i,600i,700,800,800i,900,900i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
</head>
<body>

		
<?php 
		

	?>

	<?php
	}

	public function __destruct(){
	?>


	 <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.min.js"></script>
	<script src="<?= URL?>/VIEWS/resources/JS/scripts.js"></script>
	<script src="<?= URL?>/VIEWS/resources/JS/ajax.js"></script>
	<script src="<?= URL?>/VIEWS/resources/JS/jquery-ui/jquery-ui.min.js"></script>
	<script src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	

</body>
</html>

<?php	
		}
	}

?>
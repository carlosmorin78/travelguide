<?php namespace Views;
	session_start();


	$template = new template();

	class template{

		public function __construct(){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ADMINISTRACION</title>
  	<link rel="stylesheet" href="<?= URL?>/VIEWS/resources/CSS/helpers.css">
	<link rel="stylesheet" href="<?= URL?>/VIEWS/resources/CSS/main.css">
	<link rel="stylesheet" href="<?= URL?>/VIEWS/resources/CSS/jquery-ui.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,500i,700i,800" rel="stylesheet">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
</head>
<body>
	<!--SPINER LOADER-->
	<div class="spinner">
	  <div class="bounce1"></div>
	  <div class="bounce2"></div>
	  <div class="bounce3"></div>
	</div>
	<div class="bgBlackSpiner"></div>

		
<?php 
		
		if (isset($_SESSION['user'])){require "menu.php"; } 
	?>

	<?php
	}
	public function __destruct(){
	?>


	 <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.min.js"></script>
	<script src="<?= URL?>/VIEWS/resources/JS/scripts.js"></script>
	<script src="<?= URL?>/VIEWS/resources/JS/ajax.js"></script>
	<script src="<?= URL?>/VIEWS/resources/JS/jquery-ui/jquery-ui.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="http://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB6xxSBnN0cqP0svRB9hCw4yuVvxjXGxS8&libraries=places&callback=initMap" async defer></script>



</body>
</html>

<?php	
		}
	}

?>
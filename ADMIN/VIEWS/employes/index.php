<?php 
  
  $data = $employes->index();
  
 ?>

<div class="mainContainer">

  <div class="clear"></div>
  
  <div class="containerPanel">
    <div class="headContent">
      <h4 class="text-bold">Administrador de empresas</h4>
    </div>
    <div class="mainContent">
      <div class="row">
        <div class="col-lg-2">
          <a href="newEmploye/" class="btn blue text-600 adjustli">
            <i class="material-icons">
            add_circle
            </i>  
            <span class="ml5">Nueva empresa</span>
          </a>
        </div>
      </div>
      <div class="clear"></div>
    <div class="containerTable">
      
      <div class="clear"></div>
      
      <table class="tableCategories" id="tableCategories">  
        
        <thead> 
          <tr>  
            <td>ID</td>
            <td>Nombre</td>
            <td>Direccion</td>
            <td>Categoria</td>
            <td>Acciones</td>
          </tr>
        </thead>
        <tbody> 
          <?php while($row = mysqli_fetch_array($data)){?> 
          <tr>  
            <th><?= $row['id']; ?></th>
            <th><?= $row['name']; ?></th>
            <th><?= $row['address']; ?></th>
            <th><?= $row['category']; ?></th>
           
            <th>
              <a href="editEmploye/?id=<?= $row['id']; ?>" class="little_btn bgBlue adjust ">
                <i class="material-icons s16">
                edit
                </i>
              </a>
            
              <a href="erase/?id=<?= $row['id']; ?>"  onclick="eraseEmploye(this)" class="little_btn bgRed adjust ">
                <i class="material-icons s16">
                delete
                </i>
              </a>

            </th>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>  
    </div>      
  </div>
</div>


		z<?php 
	$data = $employes->listCategories();
 ?>
<div class="mainContainer">

	<div class="clear"></div>
	<div class="containerPanel">
		<form action="<?= URL?>employes/saveEmploye/" method="post" enctype="multipart/form-data" autocomplete="off" class="">
			<div class="headContent">
				<h4 class="text-bold">Nueva empresa</h4>
			</div>

			<div class="row">
				<div class="col-lg-5 ">
					<div class="mainContent mW600">
						<div class="row">
							<div class="col-lg-10">
								<label for="name">Nombre:</label>
								<input type="text" name="name" id="name" placeholder="Cafeterias" class="inputStyle namePlace" value="" required=""> 
							</div>
							
						</div>
						<div class="clear">	</div>

						<div class="row">
							<div class="col-lg-10">
								<label for="name">Direccion:</label>
								<input type="text" name="address" id="address" placeholder="Cafeterias" class="inputStyle" value="" required=""> 
							</div>
							
						</div>

						<div class="clear"></div>

						<div class="row">
							<div class="col-lg-10">
								<label for="phone">Telefono:</label>
								<input type="text" name="phone" id="phone" placeholder="844 - 444 - 44 - 44" class="inputStyle" value="" required=""> 
							</div>
							
						</div>

						<div class="clear"></div>
						<div class="row">
							<div class="col-lg-8">
								<label for="category">Categoria:</label> 	<br>
								<select id="category" name="category" class="inputStyle" onchange="startMap();">
									<?php while ($row = mysqli_fetch_array($data)) {?>
									
									<option value="<?= $row['key_word']; ?>"><?= $row['name']?></option>
									<?php } ?>
								</select>
							</div>
							
						</div>
						<br>	
						<div class="row">
							

							<div class="col-lg-5">
								<label for="lat">Latiutud:</label>
								<input type="text" name="lat" id="lat" placeholder="Cafeterias" class="inputStyle" value="" required=""> 
							</div>
							<div class="col-lg-5">
								<label for="lng">Longitud:</label>
								<input type="text" name="lng" id="lng" placeholder="Cafeterias" class="inputStyle" value="" required=""> 
							</div>
							
						</div>
						<div class="clear"></div>

						



						<div class="clear"></div>
						<div class="row">
							<div class="col-lg-10">
								<label for="description">Descripcion:</label>
								<textarea name="description" id="description" cols="20" rows="3" placeholder="" class="inputStyle h70" required></textarea><br>

							</div>
							
						</div>
					
					</div>	
				</div>	
				<div class="col-lg-7 hidden contentMap">
					<div class="block h450 bgBlue " id="mapForm">
						<br>	<br>	
					</div>
				</div>
			</div>
			
			<div class="footerModal">
				<div class="row">
					<div class="col-lg-4 ">
						<input type="submit" value="Guardar" class="btn green">
					</div>
				</div>
			
			</div>
		</form>
</div>
 <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      
    </script>

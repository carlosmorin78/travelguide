<?php 
	$row = $categories->edit();

 ?>
<div class="mainContainer">

	<div class="clear"></div>
	<div class="containerPanel">
		<form action="<?= URL?>categories/updateC/" method="post" autocomplete="off">
			<div class="headContent">
				<h4 class="text-bold">Editar categoria</h4>
			</div>

			<div class="mainContent mW600">
				<div class="row">
					<div class="col-lg-8">
						<label for="name">Nombre:</label>

						<input type="text" name="name" id="name" placeholder="Cafeterias" class="inputStyle" required="" value="<?= $row['name'];?>"> 
						<input type="hidden" name="id" value="<?= $row['id']?>" >
					</div>
				</div>
				<div class="clear">	</div>
				<div class="row">
					<div class="col-lg-8">
						<label for="key_word">Palabra clave:</label>
						<input type="text" name="key_word" id="key_word" placeholder="bart" class="inputStyle" required="" value="<?= $row['key_word'];?>"> 
					</div>
					
				</div>

				<div class="clear"></div>
				<div class="row">
					<div class="col-lg-10">
						<label for="description">Descripcion:</label>
						<textarea name="description" id="description" cols="20" rows="3" placeholder="" class="inputStyle h70" required><?= $row['description'];?></textarea><br>

					</div>
					
				</div>
				
			</div>	
			<div class="footerModal">
				<div class="row">
					<div class="col-lg-4 ">
						<input type="submit" value="Actualizar"  class="btn green">
					</div>
				</div>
			
			</div>
		</form>
</div>
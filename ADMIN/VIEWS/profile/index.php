<?php 
	$data = $profile->index();

?>
<div class="mainContainer">

	<div class="clear"></div>
	<div class="containerPanel">
		<pre>
			<?php print_r($data);?>
		</pre>
		<div class="headContent">
			<h4 class="text-bold">Perfil de usuario</h4>
		</div>
		<div class="mainContent">
			
			<div class="row">
				<div class="col-lg-2">
					<div class="block">
						<img src="<?= URL_IMG ?>/profile.jpg">
					</div>
				</div>
				<div class="col-lg-8">
					<h2><?= $data['name'];?></h2>
					<p><?= $data['email'];?></p>
					<p><?= $data['phone'];?></p>
					<p><?= $data['address'];?></p>
					<div class="row">
						<div class=" col-lg-2">
							<a href="<?= URL?>profile/edit/" class="btn bgBlue"> Editar</a>
						</div>	

					</div>	

				</div>
			</div>
			<div class="clear"></div>
			<div class="clear"></div>
			
			<div class="clear"></div>
			<div class="clear"></div>
		
				

		</div>
	</div>

	<div class="clear"></div>
	
</div>
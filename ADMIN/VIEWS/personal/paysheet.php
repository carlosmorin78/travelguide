<?php 
	$row = $personal->index();

 ?>
<div class="mainContainer">
	<div class="containerPanel">	
		<form action="" method="post" class="saveWork" onsubmit="">
		
			<div class="headContent">
				<h4 class="text-bold">Capturar nomina de cuadrilla	</h4>
			</div>

			<div class="mainContent ">
				<div class="filters">
					<div class="row">
						<div class="col-lg-2 ">
							<label for="">Inicio</label>
							<input type="text" name="startDate" id="startDate" class="block">
						</div>

						<div class="col-lg-2 ">
							<label for="">Fin</label>
							<input type="text" name="endDate" id="endDate" class="block">
						</div>
						
					</div>
				</div>
				<div class="clear"></div>
				<div class="row">
				<div class="col-lg-12 ">
					<div class="containerTable">
						<table id="tableWorks">
							<thead>
								<tr>
									<td>ID</td>
									<td>NOMBRE</td>
									<td>Jueves</td>
									<td>Viernes</td>
									<td>Sabado</td>
									<td>Domingo</td>
									<td>Lunes</td>
									<td>Martes</td>
									<td>Miercoles</td>
									<td></td>
									<td>T extra</td>
									<td>Lugar</td>		
									<td>Acciones</td>		

								</tr>
							</thead>	
							<tbody>
								
								<?php while($row = mysqli_fetch_array($datos)){?>
									<form action="savePayshet/" class="bgBlue">
								    <tr>
										
									      	<th><?= $row['id']; ?></th>
									      	<th>
									      		<a href="<?=URL?>personal/viewWork/<?= $row['id'];?>"><?= $row['name']; ?></a>
									      	</th>
									      	<th>
									      		<select name="jueves" id="jueves" class="day">
									      			<option value="1">1</option>
									      			<option value="0.5">.5</option>
									      		</select>
									      	</th>
									      	
									      	<th>
									      		<select name="viernes" id="viernes" class="day">
									      			<option value="1">1</option>
									      			<option value="0.5">.5</option>
									      		</select>
									      	</th>
									      	<th>
									      		<select name="sabado" id="sabado" class="day">
									      			<option value="1">1</option>
									      			<option value="0.5">.5</option>
									      		</select>
									      	</th>
									      	<th>
									      		<select name="domingo" id="domingo" class="day">
									      			<option value="1">1</option>
									      			<option value="0.5">.5</option>
									      		</select>
									      	</th>
									      	<th>
									      		<select name="lunes" id="lunes" class="day">
									      			<option value="1">1</option>
									      			<option value="0.5">.5</option>
									      		</select>
									      	</th>
									      	<th>
									      		<select name="martes" id="martes" class="day">
									      			<option value="1">1</option>
									      			<option value="0.5">.5</option>
									      		</select>
									      	</th>
									      	<th>
									      		<select name="miercoles" id="miercoles" class="day">
									      			<option value="1">1</option>
									      			<option value="0.5">.5</option>
									      		</select>
									      	</th>
									      	<td></td>
									      	<td>
									      		<input type="number" class="mW80">
									      	</td>
									      	<td>
									      		<select name="place" id="place" onchange="selectPlace(this);">
									      			<option value="1">empaque</option>
									      			<option value="2">campo</option>
									      			<option value="3">destajo</option>
									      		</select>
									      		<input type="number" name="price" id="price" class="mW80 hidden" placeholder="Precio de reja" class="hidden">
									      	</td>
									      	<td>

									      		<button type="submit" onclick="saveAs(this);">Calcular</button>
									      	</td>
								      	
								    </tr>
								</form>
								<?php } ?>
								
							</tbody>
						</table>	
					</div>
				</div>
				
				</div>
			</div>

			<div class="footerModal">
				<div class="row">
					<div class="col-lg-2 ">
						<a href="../" class="text-bold">
							<i class="material-icons s14 text-bold">arrow_back_ios</i>Volver
						</a>
					</div>
				</div>
			</div>

		</form>
		
		<button onclick="calcular_total();">Calcylr</button>
		<div id="total">
			
		</div>
	</div>

</div>

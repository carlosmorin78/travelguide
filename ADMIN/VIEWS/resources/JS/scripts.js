$(document).ready(function() { 
  $("div.spinner").addClass("hidden");
  $("div.bgBlackSpiner").addClass("hidden");

 // alert("elmo rin ");

	 $( "#startDate" ).datepicker();


	//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js
  $('table#tableWorks').DataTable();
	$('table#tableCategories').DataTable();

  //SCRIPTS PARA MENU DEL ADMIN

  $("div.menuAdmin").click(function(){
    $("nav.menuNav").toggleClass("w60");
    $("div.containerPanel").toggleClass("extend");
  });


});

$(function () {
	$.datepicker.setDefaults($.datepicker.regional["es"]);
	
	///////}

    var dateFormat = "mm/dd/yy",
      from = $( "#startDate" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 1
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#endDate" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 1
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }

	//////
});
function openTab(obj,ntab) {
	//event.preventDefault();
	$("ul.tabs > li").removeClass('current');
	$(obj).addClass('current');

	$("ul.containerTabs > li").addClass("hidden");
	$("ul.containerTabs > li#"+ntab+"").removeClass("hidden");


}

function openModal(obj,name){
	$(".bgBlack").fadeIn("fast");
	$("form#"+name).fadeIn("fast");
}

function closeModal(){
	$(".bgBlack").fadeOut("fast");
	$(".modal").fadeOut("fast");
}

function calcular_total(obj) {
  event.preventDefault();
  var tr = $(obj).parent("td").parent("tr");

  importe_total = 0
  $(tr).find("select.day").each(

    function(value) {
       


      importe_total += parseFloat($(this).val());

    }
  );
  alert(importe_total * 160);
 // $("#total").val(importe_total * 160 );
}

function selectPlace(obj){
  var val = $(obj).val();
  if ( val == 3 ) {
    $(obj).siblings("input#price").removeClass("hidden");
  }else{
    $(obj).siblings("input#price").addClass("hidden");
  }

}



      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
function startMap(obj){

  var placeMap = $("select#category").val();
  initMap(placeMap); 
  $("div.contentMap").removeClass("hidden");
}
  var map;
      var infowindow;

      function initMap(placeMap) {
        var pyrmont = {lat: 25.450, lng: -102.196};

        map = new google.maps.Map(document.getElementById('mapForm'), {
          center: pyrmont,
          zoom: 15
        });

        infowindow = new google.maps.InfoWindow();
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
          location: pyrmont,
          radius: 10000,
          type: [placeMap]
        }, callback);
      }

      function callback(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
          }
        }
      }

      function createMarker(place) {
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
        });

        google.maps.event.addListener(marker, 'click', function() {
          $("input#Lat").val(placeLoc.lat());
          $("input#Lng").val(placeLoc.lng());


          infowindow.setContent(place.name);
          infowindow.open(map, this);
        });
      }


      

<nav class="menuNav">
	
	<ul>
		<li class="bgDblue">
			<div class="row ">
				<div class=" col-lg-8">
					<div class="block h50">
						<div class="content middle">
							<img src="<?= URL_IMG?>/logo.png" height="50">
						</div>
					</div>
				</div>
				<div class="col-lg-4 menuAdmin noPadding">
					<div class="block h50 ">
						<div class="content middle cWhite text-center">
							<i class="material-icons s30">
							menu
							</i>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li class="porfileInfo text-center bgDblue">
			<div class="row">
				<div class="col-lg-4 ">
					<div class="block h80">
						<div class="content middle">
							<div class="imgProfile" style="background-image: url('<?=URL.'VIEWS/resources/IMG/profile.jpg'?>');">
						<br>
					</div>	
						</div>
					</div>
					
				</div>
				<div class="col-lg-8" >
					<div class="block h80">
						<div class="content middle text-left">
							<label class="s10 cWhite text-left text-600">Usuario:</label> <br>
							<span class="s12 cWhite"><?= $_SESSION['user'][0]['name']; ?></span> <br>
							<label class="s10 cWhite text-left text-600">Email:</label> <br>
							<span class="s12 cWhite"><?= $_SESSION['user'][0]['email']; ?></span>
						
						</div>
					</div>
					
				</div>
			</div>
			
		</li>
		<li class="headermenun ">
			<a href="<?=URL?>profile/" class="adjustli">
				
				<i class="material-icons s20 w60 text-center">
				account_circle
				</i>
				<span class="w100 ">Perfil</span>
			</a>
		</li>
		<li>
			<a href="<?=URL?>categories/" class="adjustli">
				<i class="material-icons s20 w60 text-center">
				ballot
				</i>
				<span class="w100 ">Categorias</span>
			</a>
		</li>
		<li>
			<a  href="<?=URL?>employes/" class="adjustli">
				<i class="material-icons s20 w60 text-center">
				business
				</i>
				<span class="w100 ">Empresas</span>
			</a>
		</li>
		<li>
			<a  href="<?=URL?>login/logout/" class="adjustli">
				<i class="material-icons s20 w60 text-center">
				forward
				</i>
				<span class="w100 ">Salir</span>
			</a>
		</li>
	</ul>
</nav>	
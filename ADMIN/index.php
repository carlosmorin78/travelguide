<?php 
	define('DS', DIRECTORY_SEPARATOR);
	define('ROOT', realpath(dirname(__FILE__)) . DS);
	define('URL', "http://localhost/traverGuide/ADMIN/");
	define('URL_IMG', "http://localhost/traverGuide/ADMIN/VIEWS/resources/IMG/");

	require_once "CONFIG/autoload.php";
	
	require_once "VIEWS/TEMPLATE.PHP";
	Config\autoload::run();

	Config\enrutador::run(new Config\request());

 ?>
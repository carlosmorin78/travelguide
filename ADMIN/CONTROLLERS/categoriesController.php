<?php 
	namespace CONTROLLERS;
	
	use MODELS\categories as categories;

	class categoriesController
	{
		public $categories;
		public $name;
		public $description;
		public $image;
		public $file;
		public $new;
		public $id;
		

		public function __construct(){
			$this->categories = new categories();
		}

		public function index(){
			$data = $this->categories->listar();
			return $data;
		}
	
		
		public function newCategories(){
			return false;
		}

		public function edit(){
			$this->categories->set("id", $_GET['id']);
			$row = $this->categories->listId();
			return $row;
		}


		public function addC(){

			$this->categories->set("name", $_POST['name']);
			$this->categories->set("key_word", $_POST['key_word']);
			$this->categories->set("description", $_POST['description']);
			$this->categories->set("img", $_FILES['image']['name']);
			$this->categories->newCat();
				
			$currentDir = getcwd();	
		
		    $uploadDirectory = "/VIEWS/resources/IMG/categories/";

		    $errors = []; // Store all foreseen and unforseen errors here

		    $fileExtensions = ['jpeg','jpg','p|ng']; // Get all the file extensions

		    $fileName = $_FILES['image']['name'];
		    $fileSize = $_FILES['image']['size'];
		    $fileTmpName  = $_FILES['image']['tmp_name'];
		    $fileType = $_FILES['image']['type'];
		    $fileExtension = strtolower(end(explode('.',$fileName)));

		    $uploadPath = $currentDir . $uploadDirectory . basename($fileName); 

		    if (isset($_POST)) {

		        if (! in_array($fileExtension,$fileExtensions)) {
		            $errors[] = "This file extension is not allowed. Please upload a JPEG or PNG file";
		        }

		        if ($fileSize > 2000000) {
		            $errors[] = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
		        }

		        if (empty($errors)) {
		            $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

		            if ($didUpload) {
		                echo "The file " . basename($fileName) . " has been uploaded";
		            } else {
		                echo "An error occurred somewhere. Try again or contact the admin";
		            }
		        } else {
		            foreach ($errors as $error) {
		                echo $error . "These are the errors" . "\n";
		            }
		        }
		    }
		    header("Location: ".URL."categories/");

		}

		public function delete(){
			$this->categories->set("id", $_GET['id']);
			$this->categories->delete();
			header("Location: ".URL."categories/");
		}

		public function updateC(){
			if ($_POST) {
				$this->categories->set("id", $_POST['id']);
				$this->categories->set("name", $_POST['name']);
				$this->categories->set("key_word", $_POST['key_word']);
				$this->categories->set("description", $_POST['description']);
				$this->categories->update();
				header("Location: ".URL."categories/");

			}
		}

	
		

		
		
	}

	$categories = new categoriesController;

 ?>	


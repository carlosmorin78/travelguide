<?php 
	namespace Controllers;
	
	use MODELS\employes as employes;

	class employesController
	{
		private $employes;
		private $name;
		private $address;
		private $lat;
		private $lng;
		private $category;
		private $description;
		private $phone;
		private $id;

		public function __construct(){
			$this->employes = new employes();
		}

		public function index(){
			$data = $this->employes->listEmployes();
			return $data;
		}

		public function listCategories(){
			$data = $this->employes->listarCategories();
			return $data;
		}
		public function newEmploye(){
			return false;
		}

		public function saveEmploye(){
			if ($_POST) {
				$this->employes->set("name", $_POST['name']);
				$this->employes->set("address", $_POST['address']);
				$this->employes->set("lat", $_POST['lat']);
				$this->employes->set("lng", $_POST['lng']);
				$this->employes->set("category", $_POST['category']);
				$this->employes->set("description", $_POST['description']);
				$this->employes->set("phone", $_POST['phone']);
				$this->employes->save();
				header("Location: ".URL."employes/");

				
			}
			
		}

		public function erase(){
			$this->employes->set("id", $_GET['id']);
			$this->employes->delete();
			header("Location: ".URL."employes/");
		}

		public function editEmploye(){
			$this->employes->set("id", $_GET['id']);
			$data = $this->employes->listId();
			return $data;
		}
	
		public function updateEmploye(){
			if ($_POST) {
				$this->employes->set("id", $_POST['id']);
				$this->employes->set("name", $_POST['name']);
				$this->employes->set("address", $_POST['address']);
				$this->employes->set("lat", $_POST['lat']);
				$this->employes->set("lng", $_POST['lng']);
				$this->employes->set("category", $_POST['category']);
				$this->employes->set("description", $_POST['description']);
				$this->employes->set("phone", $_POST['phone']);
				$this->employes->update();
				header("Location: ".URL."employes/");

				
			}
		}

	
		

		
	}

	$employes = new employesController;

 ?>	


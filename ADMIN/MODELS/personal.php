<?php 
	namespace MODELS;

	use MODELS\cuadrillas as cuadrillas;

	class personal extends cuadrillas
	{
		public $id;
		public $name;
		public $con;
		public $value;
		public $row = array();
		public $datos = array();
		public $array = array();
		public $liable;
		public $nPeople;
		public $coments;
		//PARAMETROS DE addWork
		public $second_name;
		public $last_name;
		public $second_last_name;
		public $date_bird;
		public $origin_place;
		public $diaseases;
		public $curp;
		public $salary_day;
		public $contract_time;
		public $cuadrilla;
		public $start_date;
		public $advance;

		public $id_work;
		public $date;
		public $jueves;
		public $viernes;
		public $sabado;
		public $domingo;
		public $lunes;
		public $martes;
		public $miercoles;
		
		

		public function __construct(){

			$this->con = new conexion();

		}

		public function set($atributo, $contenido){

			$this->$atributo = $contenido;
		}

		public function get($atributo){

			return $this->$atributo;
		}


		public function listar(){	
			$sql = "SELECT * FROM personal WHERE cuadrilla = '{$this->id}'";
			$datos = $this->con->consultaRetorno($sql);
			return $datos;
		}

		public function deleteWork(){
			$sql = "SELECT * FROM personal";
			$datos = $this->con->consultaRetorno($sql);
			return $datos;
		}


		public function addWork(){ 
			$sql = "INSERT INTO personal (id, name, second_name, last_name, second_last_name, date_bird, origin_place,diaseases, status, curp, salary_day, contract_time, advance, cuadrilla, start_date) 
					VALUES (null,'{$this->name}','{$this->second_name}','{$this->last_name}','{$this->second_last_name}','{$this->date_bird}','{$this->origin_place}','{$this->diaseases}',1,'{$this->curp}','{$this->salary_day}','{$this->contract_time}','{$this->advance}','{$this->cuadrilla}','{$this->start_date}')";
			$this->con->consultaSimple($sql);
			error_log("si entro al modelo");
			//header("Location: ".URL."personal/?tab=1");
		}

		public function eraseWork(){
			$sql = "DELETE FROM personal WHERE id = '{$this->id}'";
			$this->con->consultaSimple($sql);
		} 

		public function selectWork(){
			$sql = "SELECT * FROM personal WHERE id = '{$this->id}'";
			$datos = $this->con->consultaRetorno($sql);
			$row = mysqli_fetch_assoc($datos);
			return $row;
		}

		public function update(){
			
			$sql = "UPDATE personal SET name ='{$this->name}',
							second_name ='{$this->second_name}',
							last_name ='{$this->last_name}',
							second_last_name ='{$this->second_last_name}',
							date_bird='{$this->date_bird}',
							origin_place='{$this->origin_place}',
							diaseases='{$this->diaseases}',
							status='{$this->status}',
							curp='{$this->curp}',
							salary_day='{$this->salary_day}',
							contract_time='{$this->contract_time}',
							cuadrilla='{$this->cuadrilla}' WHERE id={$this->id}";
			$this->con->consultaSimple($sql);
			
		}

		public function searchwork(){
			//error_log("si entro al modelo");
			session_start();
			$sql = "SELECT * FROM personal WHERE curp = '{$this->curp}'";
			$datos = $this->con->consultaRetorno($sql);
			$row = mysqli_fetch_array($datos);
			$numfilas = $datos->num_rows;
			//	error_log("El número de elementos es ".$numfilas);
			
			$array = array("data" => array($row),
							"nrows" => $numfilas); 
			$_SESSION['data'] = $array;
			error_log(print_r($array,true));

			return $array;	
			
		}

		public function save(){
			$sql = "INSERT INTO assistance (id_work, date, jueves,viernes,sabado,domingo,lunes,martes,miercoles) 
					VALUES ('{$this->id_work}',
							'{$this->date}',
							'{$this->jueves}',
							'{$this->viernes}',
							'{$this->sabado}',
							'{$this->domingo}',
							'{$this->lunes}',
							'{$this->martes}',
							'{$this->miercoles}')";

			$this->con->consultaSimple($sql);
			
		}

		

	
	} 


 ?>
<?php 
	namespace MODELS;

	class employes 
	{
		private $employes;
		private $name;
		private $address;
		private $lat;
		private $lng;
		private $category;
		private $description;
		private $phone;
		private $id;
		public function __construct(){

			$this->con = new conexion();

		}

		public function set($atributo, $contenido){

			$this->$atributo = $contenido;
		}

		public function get($atributo){

			return $this->$atributo;
		}
		public function listarCategories(){
			
			$sql = "SELECT * FROM categories";
			$data = $this->con->consultaRetorno($sql);
			return $data;
		}
		
		public function listEmployes(){
			$sql = "SELECT * FROM employes";
			$data = $this->con->consultaRetorno($sql);
			return $data;
		}

		public function listId(){
			$sql = "SELECT * FROM employes WHERE id = '{$this->id}'";
			$data = $this->con->consultaRetorno($sql);
			$row = mysqli_fetch_array($data);
			return $row;

		}

		public function save()
		{
			$sql = "INSERT INTO employes (id, name, address, lat, lng, category, phone, description) VALUES (null, '{$this->name}','{$this->address}','{$this->lat}','{$this->lng}','{$this->category}','{$this->phone}','{$this->description}')";
			$this->con->consultaSimple($sql);
			

		}

		public function delete(){
			$sql = "DELETE FROM employes WHERE id = '{$this->id}'";
			$this->con->consultaSimple($sql);
		}
		
		public function update(){
				$sql = "UPDATE employes SET name = '{$this->name}', 
											address = '{$this->address}',
											lat = '{$this->lat}',
											lng = '{$this->lng}',
											category = '{$this->category}',
											phone = '{$this->phone}',
											description = '{$this->description}' WHERE id = {$this->id}";
			$this->con->consultaSimple($sql);

		}
		
			
				
	}

 ?>
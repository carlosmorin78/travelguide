<?php 
	namespace MODELS;

	class categories 
	{

		public $id;
		public $list;
		public $data;
		public $row;
		public $con;
		public $name;
		public $description;
		public $img;
		public $new;
		public $key_word;

		public function __construct(){

			$this->con = new conexion();

		}

		public function set($atributo, $contenido){

			$this->$atributo = $contenido;
		}

		public function get($atributo){

			return $this->$atributo;
		}

		public function listar(){
			
			$sql = "SELECT * FROM categories";
			$data = $this->con->consultaRetorno($sql);
			
			return $data;
		}
		
		public function listId(){
			$sql = "SELECT * FROM categories WHERE id = '{$this->id}'";
			$data = $this->con->consultaRetorno($sql);
			$row = mysqli_fetch_array($data);
			return $row;
		}

		public function newCat(){
			
			$sql = "INSERT INTO categories (id,name,description,img,key_word) VALUES (null, '{$this->name}','{$this->description}','{$this->img}','{$this->key_word}')";
			$this->con->consultaSimple($sql);
			
			
		}

		public function delete(){
			$sql = "DELETE FROM categories WHERE id = '{$this->id}'";
			$this->con->consultaSimple($sql);
		}

		public function update(){
			$sql = "UPDATE categories SET name = '{$this->name}', 
											description = '{$this->description}',
											key_word = '{$this->key_word}' WHERE id = {$this->id}";
			$this->con->consultaSimple($sql);
		}
		
				
	}
	$categories = new categories();

 ?>

 
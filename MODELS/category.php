<?php 
	namespace MODELS;

	class category 
	{
		public $con;
		
		//CUADRILLAS
		public function __construct(){

			$this->con = new conexion();

		}

		public function set($atributo, $contenido){

			$this->$atributo = $contenido;
		}

		public function get($atributo){

			return $this->$atributo;
		}

		public function listar(){	
		
			$sql = "SELECT * FROM categories";
			$datos = $this->con->consultaRetorno($sql);
			return $datos;
		}

				
	}

 ?>
 
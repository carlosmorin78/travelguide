<?php 
	namespace Controllers;
	use MODELS\category as category;

	class categoryController{
		private $category;

		public function __construct(){
			$this->category = new category();

		}
		
		public function index()
		{
			$data = $this->category->listar();
			return $data;
		}
		
		public function delete(){
			$this->category->set("id", $_GET['id']);
			$this->category->delete();
			
		}
		
		public function map()
		{
			$data = $this->category->listar();
			return $data;
		}


				
	}
	
	$category = new categoryController();
 ?>	